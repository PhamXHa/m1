<?php
require_once "Mage/Checkout/controllers/CartController.php";  
class Bss_AjaxCart_Checkout_CartController extends Mage_Checkout_CartController{

    public function postDispatch()
    {
        parent::postDispatch();
        Mage::dispatchEvent('controller_action_postdispatch_adminhtml', array('controller_action' => $this));
    }
    public function addAction(){
    	// var_dump($this);
    	if (!$this->_validateFormKey()) {
            $this->_goBack();
            return;
        }
        $cart   = $this->_getCart();
        $params = $this->getRequest()->getParams();
        
			$response = array();
	        try {
	            if (isset($params['qty'])) {
	                $filter = new Zend_Filter_LocalizedToNormalized(
	                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
	                );
	                $params['qty'] = $filter->filter($params['qty']);
	            }

	            $product = $this->_initProduct();
	            $related = $this->getRequest()->getParam('related_product');

	            /**
	             * Check product availability
	             */
	            if (!$product) {
	                $this->_goBack();
	                return;
	            }

	            $cart->addProduct($product, $params);
	            if (!empty($related)) {
	                $cart->addProductsByIds(explode(',', $related));
	            }

	            $cart->save();

	            $this->_getSession()->setCartWasUpdated(true);

	            /**
	             * @todo remove wishlist observer processAddToCart
	             */
	            Mage::dispatchEvent('checkout_cart_add_product_complete',
	                array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
	            );

	           if (!$cart->getQuote()->getHasError()){
						$message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->htmlEscape($product->getName()));
						$response['status'] = 'SUCCESS';
						$response['message'] = $message;
						//New Code Here
						$this->loadLayout();
						$toplink = $this->getLayout()->getBlock('top.links')->toHtml();
						$sidebar_block = $this->getLayout()->createBlock('checkout/cart_minicart')->setTemplate('checkout/cart/minicart.phtml');
						$content_sidebar= $this->getLayout()->createBlock('checkout/cart_sidebar')->setTemplate('checkout/cart/minicart/items.phtml');
						Mage::register('referrer_url', $this->_getRefererUrl());
						$sidebar = $sidebar_block->toHtml();
						$content_sb = $content_sidebar->toHtml();
						$response['toplink'] = $toplink;
						$response['sidebar'] = $sidebar;
						$response['content_sb'] = $content_sb;
					}
	        } catch (Mage_Core_Exception $e) {
	           $msg = "";
					if ($this->_getSession()->getUseNotice(true)) {
						$msg = $e->getMessage();
					} else {
						$messages = array_unique(explode("\n", $e->getMessage()));
						foreach ($messages as $message) {
							$msg .= $message.'<br/>';
						}
					}
					$response['status'] = 'ERROR';
					$response['message'] = $msg;
	        } catch (Exception $e) {
	            $response['status'] = 'ERROR';
					$response['message'] = $this->__('Cannot add the item to shopping cart.');
					Mage::logException($e);
	        }
	        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
			return;

    }


}
				