<?php
/**
 * FAQ accordion for Magento

 */

/**
 * FAQ accordion for Magento
 *
 * Website: https://www.bssgeek.com 
 * Email: support@bssgeek.com
 */
class Bss_Faq_Model_Mysql4_Faq_CustomerGroup
{
    public function ListCustomGroup()
    {
       $customer = Mage::getModel('customer/group')->getCollection();


        foreach ($customer as $code => $label) {
            $options[] = array(
             'value' => $code,
             'label' => $label
         );
        }
        
        return $options;
    }
}
