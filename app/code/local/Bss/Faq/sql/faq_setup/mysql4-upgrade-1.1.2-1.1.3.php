<?php
/**
 * FAQ accordion for Magento

 */

$installer = $this;

$installer->startSetup();

$installer->getConnection()
->addColumn(
  $installer->getTable('bss_faq/faq'),
  'customer_id',
  array(
   'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER, 
   'nullable'  => true,
   'length'    => 255,
   'default'   => null,
   'comment'   => 'Customer Id'));
$installer->getConnection()
->addColumn(
  $installer->getTable('bss_faq/faq'),
  'customer_name',
  array(
   'type'      => Varien_Db_Ddl_Table::TYPE_TEXT, 
   'nullable'  => true,
   'length'    => 255,
   'default'   => null,
   'comment'   => 'Customer Name'
 )
);          

$installer->endSetup();
