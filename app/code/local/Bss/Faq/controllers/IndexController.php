<?php
/**
 * FAQ accordion for Magento

 */

/**
 * FAQ accordion for Magento
 *

 * Website: https://www.bssgeek.com 
 * Email: support@bssgeek.com
 */
class Bss_Faq_IndexController extends Mage_Core_Controller_Front_Action
{
	/**
	 * Displays the FAQ list.
	 */
	public function indexAction()
	{
		$this->loadLayout()->renderLayout();
	}
	
	/**
	 * Displays the current FAQ's detail view
	 */
	public function showAction()
	{	

		$this->loadLayout()->renderLayout();
	}
	public function formAction()
	{
		
		$customerSession = Mage::getSingleton('customer/session');
		// var_dump($this->checkCustomerHaveOder($customerSession));die;
		if($customerSession->isLoggedIn() && $this->checkCustomerGroup($customerSession)){
			if (isset($_POST['submit'])) {
				if ($this->checkConfigOderEnable()) {
					if (!$this->checkCustomerHaveOder($customerSession)) {
						Mage::getSingleton('core/session')->addWarning("You need oder to send this data.");
						$this->_redirect('faq/index');
						return;
					}
				}
				try {
					$customer=  $customerSession->getCustomer();
					$data = $this->getRequest()->getPost();

					$data['is_active']=0;
					$data['stores']= Mage::app()->getStore()->getId();
					$data['customer_id']= $customer->getId();
					$data['customer_name']= $customer->getFirstname()." ".$customer->getLastname();
					$model = Mage::getModel('bss_faq/faq');
					$model->setCreationTime(Mage::getSingleton('core/date')->gmtDate());
					$model->setData($data);
					$model->save();
					Mage::getSingleton('core/session')->addSuccess("Add sussess.");
					$this->_redirect('faq/index');
					
				} catch (Exception $e) {
					$this->loadLayout()->renderLayout();
				}
				
				
			}else{
				$this->loadLayout()->renderLayout();
			}
			
		} else
		{
			Mage::getSingleton('core/session')->addWarning("You do not belong to the group of customers that are entitled to send.");
			$this->_redirect('faq/index');
			return;
		}

	}
	public function checkCustomerGroup($customerSession){
		$customerGroup = $customerSession->getCustomerGroupId();
		$configCustomerGroup = Mage::getStoreConfig('BSS/general/selected_customer');
		$configCustomerGroup= explode(",", $configCustomerGroup);
		return (in_array($customerGroup,$configCustomerGroup) ? true : false);
	}

	public function checkConfigOderEnable(){
		$configOderEnable = Mage::getStoreConfig('BSS/general/enable');
		if ($configOderEnable==1) {
			return true;
		}
		return false;
	}

	public function checkCustomerHaveOder($customerSession){
		$customerId = $customerSession->getCustomerID();
		$orderCollection = Mage::getResourceModel('sales/order_collection')
        ->addFieldToSelect('*')
        ->addAttributeToFilter('customer_id', $customerId);
        if ($orderCollection->getSize()>0) {
        	return true;
        }
        return false;
	}
}
