<?php 
class Bss_Faq_Block_Frontend_Form extends Mage_Core_Block_Template{
		public function getCategoryCollection()
	{
	    $categories = $this->getData('category_collection');
	    if (is_null($categories)){
    	    $categories =  Mage::getResourceSingleton('bss_faq/category_collection')
    	       ->addStoreFilter(Mage::app()->getStore())
    	       ->addIsActiveFilter();
    	    $this->setData('category_collection', $categories);
	    }
	    return $categories;
	}
	

}

 ?>