<?php
/**
 * FAQ accordion for Magento

 */

/**
 * FAQ accordion for Magento
 *
 * Website: https://www.bssgeek.com 
 * Email: support@bssgeek.com
 */
class Bss_Faq_Block_Adminhtml_Category_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Constructs current object
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('faq_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('bss_faq')->__('Category Information'));
    }
    
    /**
     * Prepares the page layout
     * 
     * Adds the tabs to the left tab menu.
     * 
     * @return Bss_Faq_Block_Adminhtml_Category_Edit_Tabs
     */
    protected function _prepareLayout()
    {
        $return = parent::_prepareLayout();

        $this->addTab(
            'main_section', 
            array(
                'label' => Mage::helper('bss_faq')->__('General information'),
                'title' => Mage::helper('bss_faq')->__('General information'),
                'content' => $this->getLayout()->createBlock('bss_faq/adminhtml_category_edit_tab_form')->toHtml(),
                'active' => true,
            )
        );
        
        return $return;
    }
}
