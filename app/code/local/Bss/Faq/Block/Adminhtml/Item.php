<?php
/**
 * FAQ accordion for Magento

 */

/**
 * FAQ accordion for Magento
 *
 * Website: https://www.bssgeek.com 
 * Email: support@bssgeek.com
 */
class Bss_Faq_Block_Adminhtml_Item extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * Constructor for FAQ Adminhtml Block
     */
    public function __construct()
    {
        $this->_blockGroup = 'bss_faq';
        $this->_controller = 'adminhtml_item';
        $this->_headerText = Mage::helper('bss_faq')->__('Manage FAQ Items');
        $this->_addButtonLabel = Mage::helper('bss_faq')->__('Add New FAQ Item');
        
        parent::__construct();
    }

    /**
     * Returns the CSS class for the header
     * 
     * Usually 'icon-head' and a more precise class is returned. We return
     * only an empty string to avoid spacing on the left of the header as we
     * don't have an icon.
     * 
     * @return string
     */
    public function getHeaderCssClass()
    {
        return '';
    }
}
